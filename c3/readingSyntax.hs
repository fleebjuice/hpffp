-- Reading syntax
-- 1:
-- a) fine, needs no change
-- b) (++) [1,2,3] [4,5,6]
-- c) fine, needs no change
-- d) fine, needs no change (if an array of a single string was intentional)
-- e) "Hello" !! 4
-- f) fine, needs no change
-- g) take 4 "lovely"
-- h) fine, needs no change
--
-- 2:
-- 1  | 2
-- -------
-- a) = d)
-- b) = c)
-- c) = e)
-- d) = a)
-- e) = b)

-- Building functions
-- 1:
-- a) let exclaim s = input where input = s ++ "!"
-- b) let gety s = input where input = take 1 $ drop 4 s
-- c) let lastword s = input where input = drop 9 s
--
-- 2:
exclaim s = input where input = s ++ "!"
gety s = input where input = take 1 $ drop 4 s
lastword s = input where input = drop 9 s
--
-- 3:
take3rdChar :: String -> Char
take3rdChar s = head $ drop 2 s
--
-- 4:
letterIndex :: Int -> Char
letterIndex n = head $ drop n "Curry is awesome"
--
-- 5:
rvrs s = awesome ++ is ++ curry
  where awesome = drop 9 s ++ " "
        is = take 3 $ drop 6 s
        curry = take 5 s
--
-- 6:
-- see rvrs.hs
-- runhaskell rvrs.hs
-- should give the output:
-- "awesome is Curry"
