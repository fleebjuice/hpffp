import Data.Time

data DatabaseItem = DbString String
                  | DbNumber Integer
                  | DbDate   UTCTime
                  deriving (Eq, Ord, Show)

theDatabase :: [DatabaseItem]
theDatabase =
  [ DbDate (UTCTime
            (fromGregorian 1911 5 1)
            (secondsToDiffTime 34123))
  , DbNumber 9001
  , DbString "Hello, world!"
  , DbDate (UTCTime
            (fromGregorian 1921 5 1)
            (secondsToDiffTime 34123))
  ]

-- 1. Write a function that filters for DbDate values and returns
-- a list of the UTCTime values inside them.

d :: DatabaseItem
d = DbDate (UTCTime
            (fromGregorian 0 0 0)
            (secondsToDiffTime 34123))

f a b
  | a > d     = [a] ++ b
  | otherwise = b

filterDbDate :: [DatabaseItem] -> [DatabaseItem]
filterDbDate []   = []
filterDbDate list = foldr f [] list

-- 2. Write a function that filter for DbNumber values and returns
-- a list of Integer values inside them.

i :: DatabaseItem
i = DbNumber 0

fInt a b
  | a > i     = [a] ++ b
  | a > i     = [a] ++ b
  | otherwise = b

filterDbNumber :: [DatabaseItem] -> [Integer]
filterDbNumber list = undefined -- foldr fInt (0 :: Integer) list

-- I can see a solution here.. but it's not pretty:
-- The only way I can see this happening with the knowledge of the
-- book so far involves defining a function similar to f which
-- filters out everything other than DbNumber, running show on
-- each DbNumber, dropping the name and space (so that only the
-- Integer number is left in String form), and then running read
-- on the result, and adding that to the list of ints. This sounds
-- very dodgy and I'm not sure if I'm barking up the wrong tree
-- or not, so am reluctant to implement, given the advice to not
-- use read, ever.
