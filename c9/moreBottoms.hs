import Data.Bool (bool)
-- 1. Bottom (1st value/leaf in the list is undefined, explodes when evaluated
--
-- 2. Returns [2]
--
-- 3. Will return '[2,*** exeption...': succeeds at mapping 1st value,
-- throws when evaluating 2nd value
--
-- 4. What does the following mystery function do? What is its type? Describe
-- it in standard English.

itIsMystery xs = map (\x -> elem x "aeiou") xs

-- The above function has the type :: String -> [Bool]
-- itIsMystery is passed a list of Chars, and the lambda function gets applied
-- to each of the Chars. The lambda function is passed a Char, which then gets
-- passed to elem, along with a list of vowel Chars. Each Char is essentially
-- getting checked to see if it is a vowel. Assuming the entire list is
-- evaluated by itIsMystery, a list of Bools will be returned, each of which
-- correspond to whether its equivalent input Char was a vowel or not (true
-- if vowel).
--
-- For example, given the input String:
-- "one two three"
-- The expected output will be:
-- [True, False, True, False, False, False, True, False, False, False, False, True, True]
--
-- 5. What will be the result of the following functions:
-- 
-- a) map (^2) [1..10]
--    [1,4,9,16,25,36,49,64,81,100]
--    
-- b) map minimum [[1..10], [10..20], [20..30]]
--    [1,10,20]
--
-- c) map sum [[1..5], [1..5], [1..5]]
--    [15, 15, 15]
--
-- 6. Write the 'negate -3' map function shown earlier using Data.Bool's bool
-- function:
negThe3 = map (\x -> bool (x) (-x) (x == 3)) [1..10]
