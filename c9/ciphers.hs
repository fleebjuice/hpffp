module Cipher where

import Data.Char

shiftNumber :: Int
shiftNumber = 10

numberOfLetters :: Int
numberOfLetters = (ord 'z') - (ord 'a') + 1  -- +1 because we want to start counting at 1

normalise :: Char -> Int
normalise c = (ord c) - 97

normalToChar :: Int -> Char
normalToChar a = chr $ a + 97

shift :: Int -> Int
shift n = mod (n + shiftNumber) numberOfLetters

unshift :: Int -> Int
unshift n = mod (n - shiftNumber) numberOfLetters

op :: Char -> Char
op = normalToChar . shift . normalise

unOp :: Char -> Char
unOp = normalToChar . unshift . normalise

caesar :: [Char] -> [Char]
caesar []     = []
caesar (x:xs) = (op x : caesar xs)

unCaesar :: [Char] -> [Char]
unCaesar [] = []
unCaesar (x:xs) = (unOp x : unCaesar xs)
