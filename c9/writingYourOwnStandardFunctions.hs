-- 1. myOr returns True if any Bool in the list is True

myOr :: [Bool] -> Bool
myOr []     = False
myOr (x:xs) = if x == True then True else myOr xs

-- 2. myAny returns True if a -> Bool applied to any of the values
-- in the list returns True.

myAny :: (a -> Bool) -> [a] -> Bool
myAny _ []     = False
myAny f (x:xs) = if (f x) == True then True else myAny f xs

-- 3. After you write the recursive myElem, write another version
-- that uses any.

myElem :: Eq a => a -> [a] -> Bool
myElem _ []     = False
myElem e (x:xs) = if e == x then True else myElem e xs

myElemUsingAny :: Eq a => a -> [a] -> Bool
myElemUsingAny _ [] = False
myElemUsingAny e l  = any (\x -> x == e) l

-- 4. Implement myReverse

myReverse :: [a] -> [a]
myReverse [] = []
myReverse list    = x ++ xs
  where x         = drop newlength list
        xs        = myReverse $ take newlength list
        newlength = length list - 1

-- 5. squish flattens a list of lists into a list

squish :: [[a]] -> [a]
squish []     = []
squish (x:xs) = x ++ squish xs

-- 6. squishMap maps a function over a list and concatinates
-- the results.

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap _ []     = []
squishMap f (x:xs) = f x ++ squishMap f xs

-- 7. squishAgain flattens a list of lists into a list. This time
-- re-use the squishMap function.

squishAgain :: [[a]] -> [a]
squishAgain []     = []
squishAgain (x:xs) = squishMap (\x -> [x]) x ++ squishAgain xs

-- 8. Implement maximumBy
-- N.B. not entirely sure I undersood the question (or how to 
-- conditionally solve based on last value compare returned
-- GT for), but here is *a* solution:

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy f list
  | x == LT = if (length list == 2) then (last list) else myMaximumBy f $ tail list
  | x == EQ = if (length list == 2) then (head list) else myMaximumBy f $ tail list
  | x == GT = if (length list == 2) then (head list) else myMaximumBy f (take (length list - 1) list)
  where x   = f (head list) (last list)

-- 9. Implement minimymBy
-- N.B. not entirely sure I undersood the question (or how to 
-- conditionally solve based on last value compare returned
-- LT for), but here is *a* solution:

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy f list
  | x == LT = if (length list == 2) then (head list) else myMinimumBy f (take (length list - 1) list)
  | x == EQ = if (length list == 2) then (head list) else myMinimumBy f $ tail list
  | x == GT = if (length list == 2) then (last list) else myMinimumBy f $ tail list
  where x   = f (head list) (last list)
