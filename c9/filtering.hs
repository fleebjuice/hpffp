-- Filtering
--
-- 1. Write a filter function that would give all the multiples of 3 out of a
-- list from 1-30.

multsOfThree = filter (\x -> rem x 3 == 0) [x | x <- [1..30]]

-- 2. Compose the above function with the length function to tell us *how many*
-- multiples of 3 there are between 1 and 30.
multsOfThreeComposed = length . (filter (\x -> rem x 3 == 0)) $ [x | x <- [1..30]]

-- 3. Write a function that removes all articles ('the', 'a', 'an') from
-- sentences.
myFilter :: String -> [String]
myFilter = (filter (/="the")) . (filter (/="an")) . (filter (/="a")) . words
