-- 1. Write your won version of zip :: [a] -> [b] -> [(a, b)]
-- and ensure it behaves the same as the original.

myZip :: [a] -> [b] -> [(a, b)]
myZip []     _      = []
myZip _      []     = []
myZip (a:as) (b:bs) = (a, b) : myZip as bs

-- 2. Do what you did for zip, but now for zipWidth :: (a -> b -> c)
--   -> [a] -> [b] -> [c]

myZipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
myZipWith _ []     _      = []
myZipWith _ _      []     = []
myZipWith f (a:as) (b:bs) = f a b : myZipWith f as bs

-- 3. Rewrite your zip in terms of the zipWidth you wrote.

myRewrittenZip :: [a] -> [b] -> [(a, b)]
myRewrittenZip as bs = myZipWith (\x -> \y -> (x, y)) as bs
