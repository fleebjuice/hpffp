-- EnumFromTo
eftBool :: Bool -> Bool -> [Bool]
eftBool False True  = [False, True]
eftBool True  True  = [True, True]
eftBool False False = [False, False]
eftBool _     _    = []

eftOrd :: Ordering -> Ordering -> [Ordering]
eftOrd a b
  | a >  b = []
  | a == b = [a]
  | a <  b = a : eftOrd (succ a) b

eftInt :: Int -> Int -> [Int]
eftInt a b
  | a >  b = []
  | a == b = [a]
  | a <  b = a : eftInt (succ a) b

eftChar :: Char -> Char -> [Char]
eftChar a b
  | a >  b = []
  | a == b = [a]
  | a <  b = a : eftChar (succ a) b
