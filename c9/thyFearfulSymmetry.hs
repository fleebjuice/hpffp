module PoemLines where

-- 1.
myWords :: [Char] -> [[Char]]
myWords "" = []
myWords str = x : xs
  where x  = takeWhile ((/=) ' ') str
        xs = myWords $ dropWhile ((==)' ') $ dropWhile ((/=)' ') str

-- 2.

firstSen = "Tyger Tyger, burning bright\n"
secondSen = "In the forests of the night\n"
thirdSen = "What immortal hand or eye\n"
fourthSen = "Could frame thy fearful symmetry?"
sentences = firstSen ++ secondSen
         ++ thirdSen ++ fourthSen

-- putStrLn sentences -- should print
-- Tyger Tyger, burning bright
-- In the forests of the night
-- What immortal hand or eye
-- Could frame thy fearful symmetry?

myLines :: String -> [String]
myLines ""  = []
myLines str = x : xs
  where x  = takeWhile ((/=) '\n') str
        xs = myLines $ dropWhile ((==)'\n') $ dropWhile ((/=)'\n') str

-- What we want 'myLines sentences' to equal
shouldEqual =
  [ "Tyger Tyger, burning bright"
  , "In the forests of the night"
  , "What immortal hand or eye"
  , "Could frame thy fearful symmetry?"
  ]

-- The main function here is a small test
-- to ensure you've written your function
-- correctly.
main :: IO ()
main =
  print $ "Are they equal? "
          ++ show (myLines sentences == shouldEqual)

-- 3.
myCommon :: Char -> String -> [String]
myCommon _     ""  = []
myCommon delim str = x : xs
  where x  = takeWhile ((/=) delim) str
        xs = myCommon delim $ dropWhile ((==) delim) $ dropWhile ((/=) delim) str

myWords2 = myCommon ' '
myLines2  = myCommon '\n'
