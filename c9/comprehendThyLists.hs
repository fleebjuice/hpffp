mySqr = [x^2 | x <- [1..5]]

-- Look at the following functions, figure out what the output lists
-- will be

-- [x | x <- mySqr, rem x 2 == 0]
-- [4,16]

-- [(x, y) | x <- mySqr, y <- mySqr, x < 50, y > 50]
-- []

-- take 5 [ (x, y) | x <- mySqr
--        , y <- mySqr, x < 50, y > 50]
-- []
