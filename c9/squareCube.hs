-- Given the following:

mySqr = [x^2 | x <- [1..5]]
myCube = [y^3 | y <- [1..5]]

-- 1. Write an expression that will make tuples out of the outputs of
-- mySqr and myCube

tuples = [(x, y) | x <- mySqr, y <- myCube]

-- 2. Alter that expression so that it only uses the x and y values that
-- are less than 50.

tuplesLT50 = [(x, y) | x <- mySqr, y <- myCube, x < 50, y < 50]

-- 3. Apply another function to that list comprehention to determine
-- how many tuples inhabit your output list.

l = length tuplesLT50
