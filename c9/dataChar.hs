-- 1. Query the types of isUpper and toUpper
-- :t isUpper
-- isUpper :: Char -> Bool
-- :t toUpper
-- toUpper :: Char -> Char
--
-- 2. Write a function such that, given the input 'HbEfLrLxO", your function
-- will return "HELLO"

import Data.Char

removeLower :: [Char] -> [Char]
removeLower str = filter isUpper str

-- 3. Write a function that will capitalize the first letter of a String and
-- return the entire String.
firstLetterToUpper :: [Char] -> [Char]
firstLetterToUpper [] = []
firstLetterToUpper (x:xs) = toUpper x : xs

-- 4. Now make a new version of that function that is recursive such that if
-- you give it the input "woot" it will holler back at you "WOOT".
allCaps :: [Char] -> [Char]
allCaps [] = []
allCaps (x:xs) = toUpper x : allCaps xs

-- 5. Write a function that will capitalise the first letter of a String
-- and return only that letter as the result.
onlyFirstLetterToUpper :: [Char] -> Char
onlyFirstLetterToUpper str = toUpper $ head str

-- 6. Now rewrite it as a composed function.
onlyFirstComposed :: [Char] -> Char
onlyFirstComposed str = toUpper . head $ str
-- Then, rewrite it pointfree.
onlyFirstPF :: [Char] -> Char
onlyFirstPF = toUpper . head
