f :: Int -> IO ()
f n
  | x n =  print "1"
  | otherwise = print "otherwise"
  where x = (\n -> n == (1 :: Int))
