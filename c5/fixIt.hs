-- 1:
module Sing where

fstString :: [Char] -> [Char]
fstString x = x ++ " in the rain"

sndString :: [Char] -> [Char]
sndString x = x ++ " over the rainbow"

sing :: [Char]
sing = if (x > y) then fstString x else sndString y
                  where x = "Singin"
                        y = "Somewhere"

-- 2:
-- the minor change is changing the '>' to '<'.
-- the new comparison will then cause the if
-- statement to result to the 'then' path
