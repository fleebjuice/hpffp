-- Parenthesization
-- 1
--2 + (2 * 3) - 1

-- 2
-- 10 ^ (1 + 1)

-- 3
-- ((2 ^ 2) * (4 ^ 5)) + 1


-- Equivalent expressions
-- 1:
-- 1 + 1 == 2 therefore same
--
-- 2:
-- 10 ^ 2 == 10 + 9 * 10 therefore same
--
-- 3:
-- 400 - 47 == (-) 37 400 therefore same
--
-- 4:
-- :info `div` class (Real a, Enum a) => Integral a
-- :info / class Num a => Fractional a
-- therefore not the same
--
-- 5:
-- * has higher precedence than + therefore
-- 2 * 5 + 18
-- is not the same as
-- 2 * (5 + 18)

-- More fun with functions
--
-- As of ghc-8, the given code works in ghci as-is, but the
-- following will work for both ghc-7 and ghc-8 ghci repls:
--
-- let z = 7
-- let y = z + 8
-- let x = y ^ 2
-- let waxOn = x * 5
--
-- 1:
-- when
-- + waxOn
-- is entered, ghci will error (parse error) due to the
-- expression incomplete (an expression containing an
-- unparenthesized '+' should include two Nums, one on
-- either side of it for the expression to be valid)
--
-- when
-- (+10) waxOn
-- is entered, 10 will be added to waxOn (1125), and the
-- resultant expression (1135) will be printed
--
-- when
-- (-) 15 waxOn
-- is entered, the expression will be evaluated as
-- (15 - 1135) and the result (-1110) will be printed
--
-- when
-- (-) waxOn 15
-- is entered, the epression will be evaluated as
-- (1125 - 15) and the result (1110) will be printed
--
-- 2:
-- let triple x = x * 3
--
-- 3:
-- supplying
-- triple waxOn
-- at the ghci prompt will cause ghci to evaluate
-- the waxOn expression (to 1125). this is then
-- provided as the x in the 'x * 3' expression. it
-- is then multiplied by 3 (in other words, that
-- expression is evaluated), and the result (3375)
-- is printed.
--
-- 4: and 5:
z = 7
x = y ^ 2
waxOn = a
  where a = x * 5
y = z + 8
-- 5:
triple x = x * 3
-- cd to this dir, run ghci, and type
-- :l parenthesization.hs
-- and then these symbols will be available in the
-- ghci instance. then run:
-- triple waxOn
-- 6:
waxOff x = triple x
-- 7:
-- waxOff 10 = 30
-- waxOff (-50) = (-150)
