awesome = ["Papuchon", "curry", ":)"]
alsoAwesome = ["Quake", "The Simons"]
allAwesome = [awesome, alsoAwesome]

-- 1:
-- type signature of length:
-- length :: [a] -> Int
-- takes one argument, a list of any type, and as it
-- counts the number of elements, the returned type
-- is an Int (as opposed to a more general Num type
-- due to the fact that a list type can only hold
-- an integral number of elemets)
--
-- 2:
-- a) 5
-- b) 3
-- c) 2
-- d) 5
--
-- 3:
-- the '6 / length [1,2,3]' will return an error.
-- this is due to the fact that length returns and
-- feeds (/) an Int, while (/) expects two Fractional
-- types.
--
-- 4:
-- 6 `div` length [1,2,3]
--
-- 5:
-- The expression returns a Bool, as it is comparing
-- (2 + 3) to (5). the ghci output confirms:
-- :t 2 + 3 == 5
-- 2 + 3 == 5 :: Bool
--
-- 6:
-- expected type is a Bool, the result is False
-- because (3 + 5) is not equal to (5)
--
-- 7:
-- length allAwesome == 2
-- will work, it's a valid expression. will return
-- True because allAwesome contains a 'list of list
-- of Strings' (ghci gives [[[Char]]]), and it has
-- two lists of Strings
--
-- length [ 1, 'a', 3', 'b' ]
-- will not work, as a list can only hold one type
-- of element, and the above expression attemtps
-- to hold both Num's and Char's
--
-- length allAwesome + length awesome
-- will work, as (+) expects two arguments with the
-- same type, and as is visible, the length function
-- is used on both Lists, which means (+) will
-- receive two Int arguments, which is valid. will
-- return 5
--
-- (8 == 8) && ('b' < 'a')
-- will work, but will evaluate to False. while its
-- a valid expression, 'b' is greater than 'a'
-- when compared, so the second half of the
-- expression is False
--
-- (8 == 8) && 9
-- will not work, because (&&) will only accept
-- arguments of type Bool, and 9 is a Num, not a
-- Bool
--
-- 8:
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome x = x == reverse x
--
-- 9:
myAbs :: Integer -> Integer
myAbs x = if (x < 0) then x - x - x else x
--
-- 10:
f :: (a,b) -> (c,d) -> ((b,d), (a,c))
f x y = ((snd x, snd y), (fst x, fst y))

-- Correcting Syntax
-- 1:
f xs = w + 1
  where w = length xs
-- 2:
-- below is the 'id' function applied to the Num '5':
-- (\ x -> x) 5
-- 3:
-- (\(x:xs) -> x) [1,2,3]
-- 4:
-- f a = fst a

-- Match the function names to their types
-- 1: c)
-- 2: b)
-- 3: a)
-- 4: d)
