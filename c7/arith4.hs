module Arith4 where

-- id :: a -> a
-- id x = x

roundTrip :: (Show a, Read a) => a -> a
roundTrip a = read (show a)

-- 5. Write a pointfree version of roundTrip
roundTripPF :: (Show a, Read a) => a -> a
roundTripPF = read . show

-- 6. Change the type of roundTrip to (Show a, Read b) => a -> b

roundTrip2 :: (Show a, Read b) => a -> b
roundTrip2 a = let x = show a
               in read x

main = do
  print (roundTrip 4)
  print (id 4)
  print (roundTripPF 4)
  print ((roundTrip2 4) :: Int)
