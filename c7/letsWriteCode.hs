-- 1. a) rewritten tensDigit:
tensDigit :: Integral a => a -> a
tensDigit x = d
  where xLast = x `div` 10
        d     = snd $ xLast `divMod` 10
--
--    b) yes - this change preserved the type signature and is
--             a referrentially transparrent change
--    c) change it so that we're getting the hundreds digit instead:
hunsD x = d2
  where xLast = x `div` 100
        d2    = xLast `mod` 10

-- 2. Implement the function of the type a -> a -> Bool -> a
--    once each using a case expression and once with a guard.
--    Here is the pattern matching version to get you started:

foldBool3 :: a -> a -> Bool -> a
foldBool3 x y True  = x
foldBool3 x y False = y

foldBool :: a -> a -> Bool -> a
foldBool x y b =
  case b of
    True  -> x
    False -> y

foldBool2 :: a -> a -> Bool -> a
foldBool2 x y b
  | b == True = x
  | otherwise = y

-- 3. Fill in the definition

f :: Integer -> Char
f a = 'b'

g :: (a -> b) -> (a, c) -> (b, c)
g f t = (f $ fst t, snd t)

-- 4.
-- see arith4.hs

