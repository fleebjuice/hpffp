module Main where

import Control.Monad (forever)
import Data.Char (toLower)
import Data.Maybe (isJust)
import Data.List (intersperse)
import System.Exit (exitSuccess)
import System.Random (randomRIO)

newtype WordList =
  WordList [String]
  deriving (Eq, Show)

allWords :: IO WordList
allWords = do
  dict <- readFile "data/dict.txt"
  return $ WordList (lines dict)

nounWords :: IO WordList
nounWords = do
  nouns <- readFile "data/nouns.txt"
  return $ WordList (lines nouns)

minWordLength :: Int
minWordLength = 5

maxWordLength :: Int
maxWordLength = 9

gameWords :: IO WordList
gameWords = do
  (WordList aw) <- allWords
  (WordList nw) <- nounWords
  return $ WordList ((filter (gameNoun nw)) (filter gameLength aw))
  where gameLength w =
          let l = length (w :: String)
          in  l > minWordLength && l < maxWordLength
        gameNoun nw w = not $ elem (w :: String) nw

randomWord :: WordList -> IO String
randomWord (WordList wl) = do
  randomIndex <- randomRIO (0, (length wl) - 1)
  return $ wl !! randomIndex

randomWord' :: IO String
randomWord' = gameWords >>= randomWord

type GuessCount = Int

data Puzzle = Puzzle String [Maybe Char] [Char] GuessCount

instance Show Puzzle where
  show (Puzzle _ discovered guessed guessCount) =
    (intersperse ' ' $ fmap renderPuzzleChar discovered)
    ++ " Guesssed so far: " ++ guessed

renderPuzzleChar :: Maybe Char -> Char
renderPuzzleChar Nothing     = '_'
renderPuzzleChar (Just char) = char

freshPuzzle :: String -> Puzzle
freshPuzzle word = Puzzle word ([Nothing | x <- [1..(length word)]]) "" 0

charInWord :: Puzzle -> Char -> Bool
charInWord (Puzzle word _ _ _) char = elem char word

alreadyGuessed :: Puzzle -> Char -> Bool
alreadyGuessed (Puzzle _ _ already _) char = elem char already

fillInCharacter :: Puzzle -> Char -> Puzzle
fillInCharacter (Puzzle word filledInSoFar s gc) c =
  Puzzle word newFilledInSoFar (c : s) newGuessCount
  where newFilledInSoFar =
          zipWith (zipper c) word filledInSoFar
        zipper guessed wordChar guessChar =
          if wordChar == guessed
          then Just wordChar
          else guessChar
        newGuessCount =
          if elem c word
          then gc
          else gc + 1

gameOver :: Puzzle -> IO ()
gameOver (Puzzle wordToGuess _ guessed gc) =
  if gc > (length wordToGuess) then
    do putStrLn "You lose!"
       putStrLn $ "The word was: " ++ wordToGuess
       exitSuccess
  else return ()

gameWin :: Puzzle -> IO ()
gameWin (Puzzle _ filledInSoFar _ _) =
  if all isJust filledInSoFar then
    do putStrLn "You win!"
       exitSuccess
  else return ()

runGame :: Puzzle -> IO ()
runGame puzzle = forever $ do
  gameWin puzzle
  gameOver puzzle
  putStrLn $ "Current puzzle is: " ++ show puzzle
  putStr "Guess a letter: "
  guess <- getLine
  case guess of
    [c] -> handleGuess puzzle (toLower c) >>= runGame
    _   -> putStrLn "Your guess must\
                    \ be a single character"

handleGuess :: Puzzle -> Char -> IO Puzzle
handleGuess puzzle guess = do
  putStrLn $ "Your guess was: " ++ [guess]
  case (charInWord puzzle guess
      , alreadyGuessed puzzle guess) of
    (_, True) -> do
      putStrLn "You already guessed that\
                \ character, pick something else!"
      return puzzle
    (True, _) -> do
      putStrLn "This character was in the word,\
               \ filling in the word accordingly"
      return (fillInCharacter puzzle guess)
    (False, _) -> do
       putStrLn "This character wasn't in\
                \ the word, try again."
       return (fillInCharacter puzzle guess)

main :: IO ()
main = do
  word <- randomWord'
  let puzzle = freshPuzzle (fmap toLower word)
  runGame puzzle


-- Chapter 13 exercises: fixing up hangman weirdness
--
-- * although it can play with words up to 9 characters
--   long, you only get to guess 7 characters...
--
--   .. well, to be fair to the game, the number of body
--   parts you draw in order to hang the man stayed
--   pretty consistant to be fair, but to do the work
--   justice, the # of guesses has been changed to the
--   length of the word, in order to be liniarly
--   consistent in terms of difficulty.
--
--   * it ends the game after 7 guesses, whether they were
--   correct or incorrect...
--
--   ... added a new type to the Puzzle data which keeps
--   track of the number of failed guesses and modified
--   the game logic to compare this new data against the
--   length of the word.
--
--   * if your 7th (well, now nth due to above alterations)
--   guess supplies the last letter in the word, it may
--   still tell you you lost...
--
--   ... check for success before failure!
--
--   * it picks some very strange words that you didn't
--   suspect were even in the dictionary...
--
--   ... the rules of hangman say that you don't usually
--   use certain types of words, eg, nouns. I happen to
--   have the file `propernames` in the same dir as
--   `words`. We can improve the situation somewhat by
--   filtering out the nouns. This process could be
--   repeated with other types of 'disallowed words'.
--
--   * bonus.. the game counts upper case guesses...
--
--   ... so apply toLower on the user input!
