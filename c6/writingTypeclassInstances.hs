-- Exercises: Eq Instances
-- Write the Eq instance for the datatype provided.
-- 1.
data TisAnInteger =
  TisAn Integer
instance Eq TisAnInteger where
  (==) (TisAn int) (TisAn int') =
    int == int'

-- 2.
data TwoIntegers =
  Two Integer Integer
instance Eq TwoIntegers where
  (==) (Two intOne intTwo) (Two intOne' intTwo') =
    intOne == intOne' && intTwo == intTwo'

-- 3.
data StringOrInt =
    TisAnInt Int
  | TisAString String
instance Eq StringOrInt where
  (==) (TisAnInt int) (TisAnInt int') =
    int == int'
  (==) (TisAString str) (TisAString str') =
    str == str'
  (==) (TisAnInt int) (TisAString str) =
    show int == str
  (==) (TisAString str) (TisAnInt int) =
    str == show int

-- 4.
data Pair a =
  Pair a a
instance Eq a => Eq (Pair a) where
  (==) (Pair f s) (Pair f' s') =
    f == f' && s == s'

-- 5.
data Tuple a b =
  Tuple a b
instance (Eq a, Eq b) => Eq (Tuple a b) where
  (==) (Tuple a b) (Tuple a' b') =
    a == a' && b == b'

-- 6.
data Which a =
    ThisOne a
  | ThatOne a
instance Eq a => Eq (Which a) where
  (==) (ThisOne a) (ThisOne a') =
    a == a'
  (==) (ThatOne a) (ThatOne a') =
    a == a'
  (==) (ThisOne a) (ThatOne b) =
    a == b
  (==) (ThatOne a) (ThisOne b) =
    a == b

-- 7.
data EitherOr a b =
    Hello a
  | Goodbye b
instance (Eq a, Eq b) => Eq (EitherOr a b) where
  (==) (Hello a) (Hello a') =
    a == a'
  (==) (Goodbye b) (Goodbye b') =
    b == b'
