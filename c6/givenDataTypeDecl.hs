-- Given a datatype declaration, what can we do?

data Rocks =
  Rocks String deriving (Eq, Show)

data Yeah =
  Yeah Bool deriving (Eq, Show)

data Papu =
  Papu Rocks Yeah
  deriving (Eq, Show)

-- 1. phew = Papu "chases" True
-- does not typecheck. the Papu constructor requires
-- the datatypes to preceed what they represent

truth = Papu (Rocks "chomskydoz")
             (Yeah True)
-- this typechecks. The data constructor's requirements
-- are fully satisfied
--
equalityForall :: Papu -> Papu -> Bool
equalityForall p p' = p == p'
-- this typechecks
