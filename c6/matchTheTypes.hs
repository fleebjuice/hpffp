-- 1. can not change i from (Num a) to (a). Supplying
-- the number 1 is to give a an object which has an
-- instance of the Num typeclass:
-- Prelude> let i = 1
-- Prelude> :t i
-- i :: Num t => t
--
-- 2. a) f :: Float
--       f = 1.0
--    b) can not change f from (Float a) to (Num a). Would
--       need to also supply (Fractional a).
-- 3. a) f :: Float
--    b) this works:
--       f :: Fractional a => a
--       f = 1.0
--
-- 4. a) f :: Float
--       f = 1.0
--    b) this works:
--       f :: RealFrac a => a
--       f = 1.0
--
-- 5. a) freud :: a -> a
--       freud x = x
--    b) this works:
--       freud :: (Ord a) => a -> a
--       freud x = x
-- 6. a) freud' :: a -> a
--       freud' x = x
--    b) this works:
--       freud' :: Int -> Int
--       freud' x = x
-- 7. a) myX = 1 :: Int
--       sigmund :: Int -> Int
--    b) can't change sigmund to return an (a) but then
--       return myX.
-- 8. a) myX = 1 :: Int
--       sigmund' :: Int -> Int
--       sigmund' x = myX
--    b) can't change sig, not able to make the types more
--       generic, only more specific
-- 9. a) import Data.List (sort)
--       jung :: Ord a => [a] -> a
--       jung xs = head (sort xs)
--    b) this works:
--       import Data.List (sort)
--       jung :: [Int] -> Int
--       jung xs = head (sort xs)
--
-- 10. a) import Data.List (sort)
--        young :: [Char] -> Char
--        young xs = head (sort xs)
--     b) this works:
--        import Data.List (sort)
--        young :: Ord a => [a] -> a
--        young xs = head (sort xs)
--
-- 11. a) import Data.List (sort)
--        mySort :: [Char] -> [Char]
--        mySort = sort
--
--        signifier :: [Char] -> Char
--        signifier xs = head (mySort xs)
--     b) won't work because mySort explicitly expects
--        specialised Char types
--
--
-- Type-Kwon-Do Two:
--
-- 1.
chk :: Eq b => (a -> b) -> a -> b -> Bool
-- chk f a b = a == (f b)
--
-- 2.
arith :: Num b => (a -> b) -> Integer -> a -> b
-- arith blah =
