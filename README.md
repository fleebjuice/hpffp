Haskell Programming from first principles
=========================================

This repo contains exercise answers for the [Haskell Book][1].

License
-------

This repo is [licensed](LICENSE.md) under the [3-Clause-BSD License][2]

[1]: http://haskellbook.com/
[2]: https://opensource.org/licenses/BSD-3-Clause
