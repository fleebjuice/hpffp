-- 1. Steps for reducing `dividedBy 15 2`:
--
-- dividedBy 15 2 =
-- go 15 2 0
--   
--   | 15 < 2 = ...
--   -- flase, skip this branch
--   | otherwise = go (15 - 2) 2 (0 + 1)
--   go 13 2 1
--     go (13 - 2) 2 (1 + 1)
--     -- n == 11, d == 2, count == 2
--     go 11 2 2
--       go (11 - 2) 2 (2 + 1)
--       -- n == 9, d == 2, count == 3
--       go 9 2 3
--         go (9 - 2) 2 (3 + 1)
--         -- n == 7, d == 2, count == 4
--         go 7 2 4
--           go (7 - 2) 2 (4 + 1)
--           -- n == 5, d == 2, count == 5
--           go 5 2 5
--             go (5 - 2) 2 (5 + 1)
--             -- n == 3, d == 2, count == 6
--             go 3 2 6
--               go (3 - 2) 2 (6 + 1)
--               -- n == 1, d == 2, count == 7
--               -- 1 > 2! we now hit the 1st branch!
--               dividedBy returns (quot = 7, rem = 1)

-- 2. Write a function that recursively sums all numbers from 1 to n, n being
-- the argument. eg n = 5, result would be 1 + 2 + 3 + 4 + 5 = 15
-- type should be (Eq a, Num a) => a -> a

summer :: (Eq a, Num a) => a -> a
summer 0 = 0
summer x = x + summer (x - 1)

-- 3. Write a function that multiplies two integral numbers using recursive
-- summation. The type should be (Integral a) => a -> a -> a.

mulTwo :: (Integral a) => a -> a -> a
mulTwo 0 y = 0
mulTwo x y = y + mulTwo (x - 1) y
